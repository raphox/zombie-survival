class Api::V1::SurvivorInfectionSerializer < ActiveModel::Serializer
  attributes :id

  belongs_to :survivor
  belongs_to :reporter

  link(:self) { api_v1_survivor_infection_url(object) }
end
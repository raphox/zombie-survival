class Api::V1::SurvivorSerializer < ActiveModel::Serializer
  attributes :id,
    :name,
    :age,
    :gender,
    :infected,
    :created_at,
    :updated_at

  has_many :resources do
    link :related do
      href api_v1_survivor_resources_survivors_index_url(object)
      meta count: object.resources.length
    end
  end

  link(:self) { api_v1_survivor_url(object) }
end

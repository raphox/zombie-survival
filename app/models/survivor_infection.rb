class SurvivorInfection < ApplicationRecord
  validates :survivor_id, :reporter_id, presence: true

  belongs_to :survivor
  belongs_to :reporter, class_name: "Survivor"
end

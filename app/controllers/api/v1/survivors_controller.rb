module Api
  module V1
    class SurvivorsController < Api::V1::BaseController
      before_action :set_survivor, only: [:show, :update, :destroy, :update_location]

      # GET /survivors
      def index
        params[:page]    ||= {}
        ransack_params     = params[:filter] || {}
        ransack_params[:s] = sorted_fields(params[:sort], ["created_at desc"])

        @survivors = Survivor.ransack(ransack_params).result
          .page(params[:page][:number])
          .per(params[:page][:size])

        render json: @survivors, include: params[:include]
      end

      # GET /survivors/1
      def show
        render json: @survivor, include: params[:include]
      end

      # POST /survivors
      def create
        @survivor = Survivor.new(survivor_params)

        if @survivor.save
          render json: @survivor, status: :created
        else
          render json: @survivor.errors, status: :unprocessable_entity
        end
      end

      # PATCH/PUT /survivors/1
      def update
        if @survivor.update(survivor_params)
          render json: @survivor
        else
          render json: @survivor.errors, status: :unprocessable_entity
        end
      end

      # PATCH/PUT /survivors/1/update_location
      def update_location
        if @survivor.update(survivor_params.slice(:latitude, :longitude))
          render json: @survivor
        else
          render json: @survivor.errors, status: :unprocessable_entity
        end
      end

      # DELETE /survivors/1
      def destroy
        @survivor.destroy
      end

      private
        # Use callbacks to share common setup or constraints between actions.
        def set_survivor
          @survivor = Survivor.find(params[:id])
        end

        # Only allow a trusted parameter "white list" through.
        def survivor_params
          params.require(:survivor).permit(:name, :age, :gender, :latitude, :longitude)
        end
    end
  end
end

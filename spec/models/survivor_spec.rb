require 'rails_helper'

RSpec.describe Survivor, type: :model do
  let!(:default_inventory) { create_list :resource, 5 }
  let!(:survivor) { create :survivor, name: "Raphael Araújo" }

  describe ".after_create" do
    context 'created new inventory' do
      it { expect(survivor.resources).to match_array default_inventory }
    end
  end

  describe ".location" do
    before { survivor.location 9.9999, -9.0000 }
    it { expect(survivor.latitude).to eq  9.9999 }
    it { expect(survivor.longitude).to eq -9.0000 }
  end
end

require 'rails_helper'

RSpec.describe Trade, type: :model do
  def execute(trade, give_quantity = 1, receive_quantity = 1)
    trade.give(Resource.first.id, give_quantity)
    trade.receive(Resource.first.id, receive_quantity)
    trade.save
  end

  context 'when giver has inventory' do
    let!(:default_inventory) { create_list :resource, 5 }
    let!(:giver) { create :survivor, name: "Raphael Araújo" }
    let!(:receiver) { create :survivor, name: "Renato Araújo" }
    let!(:trade) { build :trade, {
      survivor_giver_id:    giver.id,
      survivor_receiver_id: receiver.id,
      resources_giver:      [],
      resources_receiver:   []
    } }

    it 'can trade resource' do
      expect(execute(trade)).to be_truthy
    end

    it 'can trade resource with multiple resources' do
      resource_exists = Resource.first
      resource        = Resource.last

      trade.receive(resource_exists.id, 1)
      trade.give(resource.id, 1)

      receiver.increment_resource!(resource_exists.id, 1)

      expect(execute(trade)).to be_truthy
      expect(giver.count_resource(resource.id)).to eq 0
      expect(receiver.count_resource(resource.id)).to eq 2
      expect(giver.count_resource(resource_exists.id)).to eq 2
    end

    it 'can trade resource with multiple resources and different quantity' do
      resource = Resource.last

      trade.give(resource.id, 1)
      trade.give(resource.id, 3)

      giver.increment_resource!(resource.id, 3)

      expect(execute(trade)).to be_truthy
      expect(trade.resources_giver.first[:quantity]).to eq 4
      expect(giver.count_resource(resource.id)).to eq 0
      expect(receiver.count_resource(resource.id)).to eq 5
    end

    it 'can not perform trade with more units you have' do
      expect(execute(trade, 10, 1)).to be_falsey
      expect(trade.errors.messages[:resources_giver]).to include "can't be greater than 1"
    end

    it 'can perform trade with more units' do
      giver.increment_resource!(Resource.first.id, 9)

      expect(execute(trade, 10, 1)).to be_truthy
    end

    it 'can not perform trade with fewer units' do
      expect(execute(trade, 1, 10)).to be_falsey
      expect(trade.errors.messages[:resources_receiver]).to include "can't be greater than 1"
    end

    it 'can not perform trade between products with different points' do
      resource = create :resource, name: "Big points", points: 1000

      create :resources_survivors, resource: resource, survivor: receiver

      trade.give(Resource.first.id, 1)
      trade.receive(resource.id, 1)

      expect { trade.save }.to raise_error(ActiveRecord::RecordNotSaved)
      expect { trade.save! }.to raise_error(ActiveRecord::RecordNotSaved)
    end

    it 'can not perform trade when infected' do
      3.times { create :survivor_infection, survivor: giver, reporter: receiver }
      3.times { create :survivor_infection, survivor: receiver, reporter: giver }

      expect(execute(trade)).to be_falsey
      expect(trade.errors.messages[:survivor_giver]).to include "giver is infected, he can't release this action"
      expect(trade.errors.messages[:survivor_receiver]).to include "receiver is infected, he can't release this action"
    end
  end

  context 'when giver has no inventory' do
    let!(:giver) { create :survivor, name: "Raphael Araújo" }
    let!(:default_inventory) { create_list :resource, 5 }
    let!(:receiver) { create :survivor, name: "Renato Araújo" }
    let!(:trade) { build :trade, {
      survivor_giver_id:    giver.id,
      survivor_receiver_id: receiver.id,
      resources_giver:      [],
      resources_receiver:   []
    } }

    it 'can not trade resource' do
      expect(execute(trade)).to be_falsey
      expect(trade.errors.messages[:resources_giver]).to include "can't be greater than 0"
    end
  end

  context 'when receiver has no inventory' do
    let!(:receiver) { create :survivor, name: "Renato Araújo" }
    let!(:default_inventory) { create_list :resource, 5 }
    let!(:giver) { create :survivor, name: "Raphael Araújo" }
    let!(:trade) { build :trade, {
      survivor_giver_id:    giver.id,
      survivor_receiver_id: receiver.id,
      resources_giver:      [],
      resources_receiver:   []
    } }

    it 'can not trade resource' do
      expect(execute(trade)).to be_falsey
      expect(trade.errors.messages[:resources_receiver]).to include "can't be greater than 0"
    end
  end
end
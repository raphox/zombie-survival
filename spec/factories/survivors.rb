FactoryGirl.define do
  factory :survivor do
    sequence(:name) { |n| "Survivor #{n}" }
    age 31
    gender "M"
    latitude 1.5
    longitude 1.5

    factory :survivor_with_resource do
      transient do
        resource nil
      end

      after(:create) do |survivor, evaluator|
        if evaluator.resource
          create(:resources_survivors, resource: evaluator.resource, survivor: survivor, quantity: 1)
        end
      end
    end
  end
end
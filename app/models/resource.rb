class Resource < ApplicationRecord
  validates :name, presence: true, length: { minimum: 4 }, uniqueness: true

  has_many :resources_survivors, dependent: :destroy
end

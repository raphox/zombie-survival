FactoryGirl.define do
  factory :resource do
    sequence(:name) { |n| "Resource #{n}" }
    points  1
  end
end
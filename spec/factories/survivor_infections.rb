FactoryGirl.define do
  factory :survivor_infection do
    survivor
    reporter { create(:survivor, name: "Reporter 1") }
  end
end
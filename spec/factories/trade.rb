FactoryGirl.define do
  factory :trade do
    survivor_giver { create :survivor, name: "Survivor lint 01" }
    survivor_receiver { create :resource, name: "Survivor lint 02" }
    resources_giver { create_list(:resource, 2).map { |item| { resource: item, quantity: 1 } } }
    resources_receiver { create_list(:resource, 2).map { |item| { resource: item, quantity: 1 } } }

    initialize_with { new(survivor_giver_id, survivor_receiver_id) }
  end
end
require 'rails_helper'

RSpec.describe Api::V1::ReportsController, type: :controller do
  describe "GET #index" do
    it "assigns report system" do
      create_list(:resource, 5)
      create_list(:survivor, 7)
      3.times { create(:survivor_infection, survivor: Survivor.first, reporter: Survivor.last) }
      Survivor.first.increment_resource!(Resource.first, 10)

      get :index, params: {}

      expect(response).to be_success

      json = JSON.parse(response.body)

      expect(json['infected']).to eq 14.29
      expect(json['noninfected']).to eq 85.71
      expect(json['resources']).to include ({ "id" => 1, "name" => "Resource 3", "avarage" => 242.86 })
      expect(json['survivors']).to include ({ "id" => 1, "name" => "Survivor 5", "infections" => 3 })
    end
  end
end
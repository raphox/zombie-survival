class Trade
  include ActiveModel::Model



  attr_accessor :survivor_giver, :survivor_receiver, :resources_giver, :resources_receiver

  validates :survivor_giver, :survivor_receiver, presence: true
  validates :resources_giver, :resources_receiver, presence: true

  validate :validate_infected, :validate_giver_max, :validate_receiver_max

  # Initialize object setting related survivor in action
  #
  # @param survivor_giver_id [Integer] references to giver survivors
  # @param survivor_receiver_id [Integer] references to receiver survivors
  def initialize(survivor_giver_id, survivor_receiver_id)
    @resources_giver    = []
    @resources_receiver = []

    @survivor_giver    = Survivor.find(survivor_giver_id)
    @survivor_receiver = Survivor.find(survivor_receiver_id)
  end

  # Set gived resource and quantity
  #
  # @param resource_giver_id [Integer] references to relatade resource
  # @param quantity [Integer] quantity of related resource
  def give(resource_giver_id, quantity)
    resource = @resources_giver.find { |item| item[:resource].id == resource_giver_id }

    if resource
      resource[:quantity] ||= 0
      resource[:quantity]  += quantity
    else
      @resources_giver << { resource: Resource.find(resource_giver_id), quantity: quantity }
    end
  end

  # Set received resource and quantity
  #
  # @param resource_receiver_id [Integer] references to relatade resource
  # @param quantity [Integer] quantity of related resource
  def receive(resource_receiver_id, quantity)
    resource = @resources_receiver.find { |item| item[:resource].id == resource_receiver_id }

    if resource
      resource[:quantity] ||= 0
      resource[:quantity]  += quantity
    else
      @resources_receiver << { resource: Resource.find(resource_receiver_id), quantity: quantity }
    end
  end

  # Validate and execute trade action
  def save
    return false unless self.valid?

    total_giver    = 0
    total_receiver = 0

    @resources_giver.each    { |item| total_giver    += item[:resource].points * item[:quantity] }
    @resources_receiver.each { |item| total_receiver += item[:resource].points * item[:quantity] }

    if total_giver < total_receiver
      raise(ActiveRecord::RecordNotSaved.new('They must respect the price table below.', self))
    end

    Resource.transaction do
      @resources_giver.each do |item|
        @survivor_giver.decrement_resource!(item[:resource].id, item[:quantity])
        @survivor_receiver.increment_resource!(item[:resource].id, item[:quantity])
      end

      @resources_receiver.each do |item|
        @survivor_receiver.decrement_resource!(item[:resource].id, item[:quantity])
        @survivor_giver.increment_resource!(item[:resource].id, item[:quantity])
      end

      return true
    end
  end

  # Execute trade action or return exception
  def save!
    save || raise(ActiveRecord::RecordNotSaved.new("Failed to save the record", self))
  end

  # Validate if giver has quantity of resource to trade
  def validate_giver_max
    @resources_giver.each do |item|
      resource_giver = @survivor_giver.resources_survivors.find_by(resource_id: item[:resource].id)
      quantity       = resource_giver ? resource_giver.quantity : 0

      if item[:quantity] > quantity
        errors.add(:resources_giver, "can't be greater than #{quantity}")
      end
    end
  end

  # Validate if receiver has quantity of resource to trade
  def validate_receiver_max
    @resources_receiver.each do |item|
      resource_receiver = @survivor_receiver.resources_survivors.find_by(resource_id: item[:resource].id)
      quantity          = resource_receiver ? resource_receiver.quantity : 0

      if item[:quantity] > quantity
        errors.add(:resources_receiver, "can't be greater than #{quantity}")
      end
    end
  end

  # Infected survivor cant trade resources
  def validate_infected
    if @survivor_giver.infected
      errors.add(:survivor_giver, "giver is infected, he can't release this action")
    end

    if @survivor_receiver.infected
      errors.add(:survivor_receiver, "receiver is infected, he can't release this action")
    end
  end
end
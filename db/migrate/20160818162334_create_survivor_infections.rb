class CreateSurvivorInfections < ActiveRecord::Migration[5.0]
  def change
    create_table :survivor_infections do |t|
      t.references :survivor, foreign_key: true
      t.references :reporter, foreign_key: true, foreign_key: { to_table: :survivor_id }

      t.timestamps
    end
  end
end

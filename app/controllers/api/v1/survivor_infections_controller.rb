module Api
  module V1
    class SurvivorInfectionsController < Api::V1::BaseController
      before_action :filter_collection, only: [:index]
      before_action :set_survivor_infection, only: [:show, :update, :destroy]

      # GET /survivor_infections
      def index
        render json: @survivor_infections, include: params[:include]
      end

      # GET /survivor_infections/1
      def show
        render json: @survivor_infection, include: params[:include]
      end

      # POST /survivor_infections
      def create
        @survivor_infection = SurvivorInfection.new(survivor_infection_params)

        if @survivor_infection.save
          render json: @survivor_infection, status: :created
        else
          render json: @survivor_infection.errors, status: :unprocessable_entity
        end
      end

      # PATCH/PUT /survivor_infections/1
      def update
        if @survivor_infection.update(survivor_infection_params)
          render json: @survivor_infection
        else
          render json: @survivor_infection.errors, status: :unprocessable_entity
        end
      end

      # DELETE /survivor_infections/1
      def destroy
        @survivor_infection.destroy
      end

      private
        # Use callbacks to share common setup or constraints between actions.
        def set_survivor_infection
          @survivor_infection = SurvivorInfection

          # If access controller by survivor's route filter itens by survivor_id
          @survivor_infection = @survivor_infection.where(survivor_id: params[:survivor_id]) unless params[:survivor_id].blank?

          @survivor_infection = @survivor_infection.where(id: params[:id]).first
        end

        def filter_collection
          params[:page]    ||= {}
          ransack_params     = params[:filter] || {}
          ransack_params[:s] = sorted_fields(params[:sort], ["created_at desc"])

          @survivor_infections = SurvivorInfection.ransack(ransack_params).result
            .page(params[:page][:number])
            .per(params[:page][:size])

          # If access controller by survivor's route filter itens by survivor_id
          @survivor_infections = @survivor_infections.where(survivor_id: params[:survivor_id]) unless params[:survivor_id].blank?

          @survivor_infections = @survivor_infections.all
        end

        # Only allow a trusted parameter "white list" through.
        def survivor_infection_params
          _params = params.require(:survivor_infection).permit(:survivor_id, :reporter_id)

          # If access controller by survivor's route filter itens by survivor_id
          _params[:survivor_infection][:survivor_id] = params[:survivor_id] unless params[:survivor_id].blank?

          _params
        end
    end
  end
end

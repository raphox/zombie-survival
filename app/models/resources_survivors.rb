class ResourcesSurvivors < ApplicationRecord
  validates :survivor_id, :resource_id, presence: true

  belongs_to :resource
  belongs_to :survivor
end

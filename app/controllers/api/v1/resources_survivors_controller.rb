module Api
  module V1
    class ResourcesSurvivorsController < Api::V1::BaseController
      before_action :filter_collection, only: [:index]
      before_action :set_resources_survivors, only: [:show, :update, :destroy]

      # GET /resources_survivors
      def index
        render json: @resources_survivors_items, include: params[:include]
      end

      # GET /resources_survivors/1
      def show
        render json: @resources_survivors, include: params[:include]
      end

      # POST /resources_survivors
      def create
        @resources_survivors = ResourcesSurvivors.new(resources_survivors_params)

        if @resources_survivors.save
          render json: @resources_survivors, status: :created
        else
          render json: @resources_survivors.errors, status: :unprocessable_entity
        end
      end

      # POST /resources_survivors/trade
      def trade
        resources_survivors = params[:resources_survivors]
        @trade              = Trade.new(resources_survivors[:survivor_giver_id], params[:survivor_id])

        resources_survivors[:resources_giver].each do |key, item|
          item = item.is_a?(String) ? JSON.parse(item).with_indifferent_access : item

          @trade.give(item[:id], item[:quantity].to_i)
        end

        resources_survivors[:resources_receiver].each do |key, item|
          item = item.is_a?(String) ? JSON.parse(item).with_indifferent_access : item

          @trade.receive(item[:id], item[:quantity].to_i)
        end

        if @trade.save
          render json: { message: "Success" }, status: :created
        else
          render json: @trade.errors, status: :unprocessable_entity
        end
      end

      # PATCH/PUT /resources_survivors/1
      def update
        if @resources_survivors.update(resources_survivors_params)
          render json: @resources_survivors
        else
          render json: @resources_survivors.errors, status: :unprocessable_entity
        end
      end

      # DELETE /resources_survivors/1
      def destroy
        @resources_survivors.destroy
      end

      private
        # Use callbacks to share common setup or constraints between actions.
        def set_resources_survivors
          params[:page]    ||= {}
          ransack_params     = params[:filter] || {}
          ransack_params[:s] = sorted_fields(params[:sort], ["created_at desc"])

          @resources_survivors = ResourcesSurvivors.ransack(ransack_params).result
            .page(params[:page][:number])
            .per(params[:page][:size])

          # If access controller by survivor's route filter itens by survivor_id
          @resources_survivors = @resources_survivors.where(survivor_id: params[:survivor_id]) unless params[:survivor_id].blank?

          @resources_survivors = @resources_survivors.where(id: params[:id]).first
        end

        def filter_collection
          @resources_survivors_items = ResourcesSurvivors

          # If access controller by survivor's route filter itens by survivor_id
          @resources_survivors_items = @resources_survivors_items.where(survivor_id: params[:survivor_id]) unless params[:survivor_id].blank?

          @resources_survivors_items = @resources_survivors_items.all
        end

        # Only allow a trusted parameter "white list" through.
        def resources_survivors_params
          params.require(:resources_survivors).permit(:resource_id, :survivor_id, :quantity)
        end
    end
  end
end

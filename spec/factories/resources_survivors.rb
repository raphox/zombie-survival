FactoryGirl.define do
  factory :resources_survivors do
    resource
    survivor

    quantity 1
  end
end

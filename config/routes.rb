Rails.application.routes.draw do
  resources :survivor_infections
  namespace :api do
    namespace :v1 do
      resources :resources

      resources :survivors do
        patch 'update_location', on: :member
        resources :survivor_infections

        resources :resources_survivors do
          post 'trade', on: :collection
        end
      end

      resources :survivor_infections
      resources :resources_survivors

      get '/reports', to: 'reports#index', as: 'reports'
    end
  end

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end

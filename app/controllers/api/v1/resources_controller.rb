module Api
  module V1
    class ResourcesController < Api::V1::BaseController
      before_action :set_resource, only: [:show, :update, :destroy]

      # GET /resources
      def index
        params[:page]    ||= {}
        ransack_params     = params[:filter] || {}
        ransack_params[:s] = sorted_fields(params[:sort], ["created_at desc"])

        @resources = Resource.ransack(ransack_params).result
          .page(params[:page][:number])
          .per(params[:page][:size])

        render json: @resources, include: params[:include]
      end

      # GET /resources/1
      def show
        render json: @resource, include: params[:include]
      end

      # POST /resources
      def create
        @resource = Resource.new(resource_params)

        if @resource.save
          render json: @resource, status: :created
        else
          render json: @resource.errors, status: :unprocessable_entity
        end
      end

      # PATCH/PUT /resources/1
      def update
        if @resource.update(resource_params)
          render json: @resource
        else
          render json: @resource.errors, status: :unprocessable_entity
        end
      end

      # DELETE /resources/1
      def destroy
        @resource.destroy
      end

      private
        # Use callbacks to share common setup or constraints between actions.
        def set_resource
          @resource = Resource.find(params[:id])
        end

        # Only allow a trusted parameter "white list" through.
        def resource_params
          params.require(:resource).permit(:name, :points)
        end
    end
  end
end
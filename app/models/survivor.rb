class Survivor < ApplicationRecord
  MAX_INFECTIONS = 3

  validates :name, :age, :gender, presence: true
  validates :name, length: { minimum: 4 }, uniqueness: true
  validates :age, numericality: true
  validates :gender, inclusion: { in: %w(F M), message: "'%{value}' is not a valid gender" }
  validates :latitude, numericality: { greater_than_or_equal_to: -90, less_than_or_equal_to: 90, allow_blank: true }
  validates :longitude, numericality: { greater_than_or_equal_to: -180, less_than_or_equal_to: 180, allow_blank: true }

  after_create :create_inventory

  has_many :resources_survivors, dependent: :destroy
  has_many :resources, through: :resources_survivors
  has_many :infections, class_name: "SurvivorInfection", dependent: :destroy

  # Set geolocation
  #
  # @param latitude [Float]
  # @param longitude [Float]
  def location(latitude, longitude)
    self.latitude  = latitude
    self.longitude = longitude
  end

  # Get quantity of resource in self survey
  #
  # @param resource_id [Integer] references to resource
  def count_resource(resource_id)
    self.resources_survivors
      .find_by(resource_id: resource_id)
      .quantity
  end

  # Increment quantity of resource in self survey
  #
  # @param resource_id [Integer] references to resource
  # @param quantity [Interger]
  def increment_resource!(resource_id, quantity = 1)
    self.resources_survivors
      .find_by(resource_id: resource_id)
      .increment!(:quantity, quantity)
  end

  # Decrement quantity of resource in self survey
  #
  # @param resource_id [Integer] references to resource
  # @param quantity [Interger]
  def decrement_resource!(resource_id, quantity = 1)
    self.resources_survivors
      .find_by(resource_id: resource_id)
      .decrement!(:quantity, quantity)
  end

  def infected
    self.infections.count >= MAX_INFECTIONS
  end

  private
    # Create initial inventory for survivor start journey
    def create_inventory
      # Get all and set an item of each resource to survey
      Resource.all.each do |item|
        ResourcesSurvivors.create!({
          resource_id: item.id,
          survivor_id: self.id,
          quantity: 1
        })
      end
    end
end

module Api
  module V1
    class ReportsController < Api::V1::BaseController
      # GET /reports
      def index
        render json: {
          infected:    self.infected,
          noninfected: self.noninfected,
          survivors:   self.survivors,
          resources:   self.resources
        }
      end

      protected
        # Percentage of infected survivors
        def infected
          total_survivors          = Survivor.count
          total_infected_survivors = Survivor
            .where("(SELECT COUNT(*) FROM survivor_infections WHERE survivor_infections.survivor_id = survivors.id) >= ?", Survivor::MAX_INFECTIONS)
            .count

          ((total_infected_survivors * 100.0) / total_survivors).round(2)
        end

        # Percentage of non-infected survivors
        def noninfected
          total_survivors             = Survivor.count
          total_noninfected_survivors = Survivor
            .where("(SELECT COUNT(*) FROM survivor_infections
              WHERE survivor_infections.survivor_id = survivors.id) < ?", Survivor::MAX_INFECTIONS)
            .count

          ((total_noninfected_survivors * 100.0) / total_survivors).round(2)
        end

        # Survivor infections
        def survivors
          Survivor
            .select("survivors.*,
              (SELECT COUNT(*) FROM survivor_infections
                WHERE survivor_infections.survivor_id = survivors.id) AS infections_count")
            .map{ |item| {
              id:         item.id,
              name:       item.name,
              infections: item.infections_count
            } }
        end

        # Average amount of each kind of resource by survivor
        def resources
          total_survivors = Survivor.count

          Resource
            .select("resources.*,
              (SELECT SUM(quantity) FROM resources_survivors
                WHERE resources_survivors.resource_id = resources.id) AS total")
            .map{ |item| {
              id:      item.id,
              name:    item.name,
              avarage: ((item.total * 100.0) / total_survivors).round(2)
            } }
        end
    end
  end
end
class Api::V1::ResourceSerializer < ActiveModel::Serializer
  attributes :id,
    :name

  link(:self) { api_v1_resource_url(object) }
end
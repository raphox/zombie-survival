# ZSSN (Zombie Survival Social Network)

The world as we know it has fallen into an apocalyptic scenario. A laboratory-made virus is transforming human beings and animals into zombies, hungry for fresh flesh.

You, as a zombie resistance member (and the last survivor who knows how to code), was designated to develop a system to share resources between non-infected humans.

## About

It is a REST API, which will store information about the survivors, as well as the resources they own.

### Features

* **Survivor**
  * A survivor have a *name*, *age*, *gender* and *last location (latitude, longitude)*.
  * A survivor also has an inventory of resources of their own property (which you need to declare when upon the registration of the survivor).
* **Location**
  * A survivor have the ability to update their last location, storing the new latitude/longitude pair in the base.
* **Flag survivor as infected**
  * In a chaotic situation like that, it's inevitable that a survivor may get contaminated by the virus.  When this happens, we need to flag the survivor as infected.
  * An infected survivor cannot trade with others, can't access/manipulate their inventory, nor be listed in the reports (infected people are kinda dead anyway, see the item on reports below).
  * A survivor is marked as infected when at least three other survivors report their contamination.
  * When a survivor is infected, their inventory items become inaccessible (they cannot trade with others).
* **Trade items**
  * Survivors can trade items among themselves.
  * To do that, they must respect the price table below, where the value of an item is described in terms of points.
  * Both sides of the trade should offer the same amount of points. For example, 1 Water and 1 Medication (1 x 4 + 1 x 2) is worth 6 ammunition (6 x 1) or 2 Food items (2 x 3).
  * The trades themselves need not to be stored, but the items must be transferred from one user to the other.

    | Item         | Points   |
    |--------------|----------|
    | 1 Water      | 4 points |
    | 1 Food       | 3 points |
    | 1 Medication | 2 points |
    | 1 Ammunition | 1 point  |

* **Reports**
  * Percentage of infected survivors.
  * Percentage of non-infected survivors.
  * Average amount of each kind of resource by survivor (e.g. 5 waters per user).
  * Points lost because of infected survivor.

## Developers

## First time

```
rails db:migrate
rails db:seed # Create default resources of survivor
```

### How to run

```
rvm use ruby-2.3.0@zombie-survival --create
bundle install
rails db:seed
rails s -b $IP -p $PORT
```

### Generate doc for swagger-ui

```
# it will generate files in public/swagger
rake swagger:docs
```

Use Swagger-UI to read the generate (e.g. http://localhost:3000/swagger/api-docs.json) files. Download it in https://github.com/swagger-api/swagger-ui/releases.

### Tests

```
rspec
```
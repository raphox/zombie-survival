class Api::V1::ResourcesSurvivorsSerializer < ActiveModel::Serializer
  attributes :id, :resource_id, :survivor_id, :quantity, :created_at, :updated_at

  belongs_to :resource
  belongs_to :survivor

  link(:self) { api_v1_resources_survivors_url(object) }
end
